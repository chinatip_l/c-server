#include <cstdlib>
#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <csignal>
#include <cerrno>
#include <chrono>
#include <future>
#include <stdio.h>


#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.>
#endif



#define CUSTOMSIG -99

using namespace std;

bool is_prime (long x) {
	cout<<"call inner";
	for (long i=2; i<x; ++i) if (x%i==0) return false;
	cout<<"Good";
	return true;
}


void thPrint(int index){
	char a[50];
	sprintf(a,"Function Thr: %d \n",index);
	cout<<a;
}

sig_atomic_t SIG=0;

void signalHandler(int res){
	SIG=res;
	cout << "Signal Raised! SignalValue: " << SIG << '\n';
}


int _main(int argc, char *argv[])
{
	WSADATA wsa;
	SOCKET s;
	
	printf("\nInitialising Winsock...");
	if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
	{
		printf("Failed. Error Code : %d",WSAGetLastError());
		return 1;
	}
	
	printf("Initialised.");
	
	if((s = socket(AF_INET , SOCK_STREAM , 0 )) == INVALID_SOCKET)
	{
		printf("Could not create socket : %d" , WSAGetLastError());
	}

	printf("Socket created.\n");
	
	#ifdef _WIN32
	cout<<"WIN32\n";
	#else
	cout<<"LIN\n";
	#endif
	cout<<(int)future_status::deferred<<(int)future_status::timeout<<(int)future_status::ready;
	std::future<bool> fut = std::async (launch::async,is_prime,700020007);
	// do something while waiting for function to set future:
	std::cout << "checking, please wait";
	std::chrono::milliseconds span (1);
//	while(true)
//		cout<<(int)fut.wait_for(span);
//	cout<<(int)fut.wait_for(span);
	
	while (fut.wait_for(span)==std::future_status::timeout)
		std::cout << '.'<<std::flush;
	
	bool x = fut.get();
	std::cout << "\n700020007 " << (x?"is":"is not") << " prime.\n";
	
	
	void (*eventInit)(int);
	
	

	eventInit=signal(SIGINT,signalHandler);
	cout << "SignalValue: " << SIG << '\n';
	cout << "Sending signal " << CUSTOMSIG << '\n';
	perror(":");
	int ret=raise(SIGINT);
	cout << "Raise result: " << ret << "," << errno <<  '\n';
	perror(":");
	cout << "SignalValue: " << SIG << '\n';
	
    vector<thread> thrlist;
    for(int i=0;i<100;i++){
    	thread tmpthr(thPrint,i);
    	thrlist.push_back(move(tmpthr));
	}
	
	for(auto &thr : thrlist){
		thr.detach();
	}
	
	
    return EXIT_SUCCESS;
}

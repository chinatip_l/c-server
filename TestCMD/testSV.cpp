#include <winsock2.h>
#include <winsock.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inaddr.h>
#include <iostream>
#include <fstream>
#include <string>
#include <typeinfo>

#define DEFAULT_PORT    80



int main(int argc, char **argv){
	
//	char dir[2048];
//    getcwd(dir, 2048);
//    std::cout << "DIR - " << dir<<std::endl;
    
	std::string htmlHead="HTTP/1.1 200 OK\r\n" "charset=UTF-8\r\n\r\n";
	std::ifstream file("index.html");
	
	if (!file.is_open()) {
        std::cout << "Could not open the file - " << std::endl;
        exit(EXIT_FAILURE);
    }
	
	std::string str((std::istreambuf_iterator<char>(file)),std::istreambuf_iterator<char>());
	
	
	std::string tmp("");
	tmp+=htmlHead;
	tmp+=str;
//	tmp+="\r\n\0";
	char* indexhtml=new char[tmp.length()+1];
	strcpy(indexhtml,tmp.c_str());
	std::cout<<"\nstrf: \n"<<indexhtml<<"\n";

//	char response[] = "HTTP/1.1 200 OK\r\n"
////	"Content-Type: text/html; charset=UTF-8\r\n\r\n"
//	"charset=UTF-8\r\n\r\n"
//	"<html><head><title>Hello World</title></head>"
//	"<body><h1>Hello world!</h1></body></html>\r\n\0";
////HTTP/1.1 200 OKrncharset=UTF-8rnrn<html><head><title>Hello World</title></head><body><h1>Hello world!</h1></body></html>rn0
	char* response=indexhtml;
	std::cout<<"\nINDEX: "<<typeid(indexhtml).name()<<sizeof(indexhtml)-1<<strlen(indexhtml)<<"\n"<<indexhtml<<"\n";
	std::cout<<"\nPrepared: "<<typeid(response).name()<<sizeof(response)-1<<"\n"<<response<<"\n";
	
	char szBuff[2048];
	int msg_len;
	int addr_len;
	struct sockaddr_in local, client_addr;
	
	local.sin_family = AF_INET;
	local.sin_port = 80;
	local.sin_addr.s_addr=inet_addr("0.0.0.0");
	
	SOCKET sock, msg_sock;
	WSADATA wsaData;
	
	if (WSAStartup(0x202, &wsaData) == SOCKET_ERROR){
	    // stderr: standard error are printed to the screen.
	    fprintf(stderr, "WSAStartup failed with error %d\n", WSAGetLastError());
	    //WSACleanup function terminates use of the Windows Sockets DLL. 
	    WSACleanup();
	    return -1;
	}
	// Fill in the address structure
	local.sin_family        = AF_INET;
	//local.sin_addr   = &local;
	local.sin_port      = htons(DEFAULT_PORT);
	
	sock = socket(AF_INET,SOCK_STREAM, 0);  //TCp socket
	
	
	if (sock == INVALID_SOCKET){
	    fprintf(stderr, "socket() failed with error %d\n", WSAGetLastError());
	    WSACleanup();
	    return -1;
	}
	
	if (bind(sock, (struct sockaddr *)&local, sizeof(local)) == SOCKET_ERROR){
	    fprintf(stderr, "bind() failed with error %d\n", WSAGetLastError());
	    WSACleanup();
	    return -1;
	}
	
	
	//waiting the connection
	if (listen(sock, 5) == SOCKET_ERROR){
	    fprintf(stderr, "listen() failed with error %d\n", WSAGetLastError());
	    WSACleanup();
	    return -1;
	}
	
	
	printf("Waiting the connection........\n");
	
	while(1){
		addr_len = sizeof(client_addr);
		msg_sock = accept(sock, (struct sockaddr*)&client_addr, &addr_len);
		if (msg_sock == INVALID_SOCKET){
		    fprintf(stderr, "accept() failed with error %d\n", WSAGetLastError());
		    WSACleanup();
		    return -1;
		}
		
		if (msg_sock == -1){
		    perror("Unable to accept connection.");
		    continue;
		}
		
		printf("accepted connection from %s, port %d\n",
		    inet_ntoa(client_addr.sin_addr),
		    htons(client_addr.sin_port));
		
		msg_len = recv(msg_sock, szBuff, sizeof(szBuff), 0);
		
		
		
		printf("Bytes Received: %d, message: %s from %s\n", msg_len, szBuff, inet_ntoa    (client_addr.sin_addr));
		
		
		std::string msg_in=std::string(szBuff);
		int httpptr=msg_in.find("HTTP/1.1");
		std::string req=msg_in.substr(0,httpptr);
		std::string method=req.substr(0,req.find(" "));
		std::string path=req.substr(req.find(" "));
		std::cout<<path<<req<<method;
//		std::cout<<"\nPrepared: "<<strlen(response)-1<<"\n"<<response<<"\n";
		msg_len = send(msg_sock, response, strlen(response)-1, 0);
		std::cout<<"\n"<<msg_len<<"\n";
		if (msg_len == 0){
		    printf("Client closed connection\n");
		    closesocket(msg_sock);
		    return -1;
		}
		
		if (msg_len == SOCKET_ERROR){
		    fprintf(stderr, "recv() failed with error %d\n", WSAGetLastError());
		    WSACleanup();
		    return -1;
		}
		
		if (msg_len == 0){
		    printf("Client closed connection\n");
		    closesocket(msg_sock);
		    return -1;
		}
		closesocket(msg_sock);
	}
	WSACleanup();
}
